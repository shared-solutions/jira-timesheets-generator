from jira import JIRA
import re
import collections
from datetime import date, timedelta, datetime
import operator
import xlsxwriter
import os

# Configuration

from config import options

user_tuple = options['user']
target_date_format = "%Y-%m-%d"

target_dir = "./timesheets"

# Time calculations

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

start_of_month = date.today().replace(day=1)
end_of_month = start_of_month.replace(month=start_of_month.month+1, day=1) - timedelta(days=1)


# Download data from JIRA
jira = JIRA(options, basic_auth=user_tuple)

jira_projects = jira.projects()

users = dict()
projects = dict()
all_issues = dict()


def format_filename(text):
    return text.replace('/', '_')
    

for project in jira_projects:
    issues = jira.search_issues('project=' + project.key)
    project_name = project.name
    days = dict()

    for date in daterange(start_of_month, end_of_month):
        days[date.strftime(target_date_format)] = []
    
    for issue in issues:
        all_issues[issue.id] = issue
        worklogs = jira.worklogs(issue.id)
        for worklog in worklogs:
            user_name = worklog.author.displayName
            
            if user_name not in users:
                users[user_name] = dict()
                
            if project_name not in users[user_name]:
                users[user_name][project_name] = dict()

            date_string = worklog.started.split('T')[0]
            worklog_date = datetime.strptime(date_string, "%Y-%m-%d").date().strftime(target_date_format)
            if worklog_date in days.keys():
                if worklog_date not in users[user_name][project_name]:
                    users[user_name][project_name][worklog_date] = []
                users[user_name][project_name][worklog_date].append(worklog)


for user_name, projects in users.iteritems():
    for project_name in projects:
        for date_string, worklogs in users[user_name][project_name].iteritems():
            if isinstance(worklogs, list) and worklogs:
                users[user_name][project_name][date_string] = max(worklogs, key=operator.attrgetter('timeSpentSeconds'))
            
            
directory = target_dir + "/" + start_of_month.strftime("%Y-%m")             
            
if not os.path.exists(directory):
    os.makedirs(directory)

for user_name, projects in users.iteritems():
    workbook_name = format_filename(user_name + "_" + start_of_month.strftime("%B")) + ".xlsx"
    workbook = xlsxwriter.Workbook(directory + "/" + workbook_name)
    
    bold = workbook.add_format({'bold': True})
    bold.set_align('center')
    date_format = workbook.add_format({'num_format': 'yyyy-mm-dd'})
    date_format.set_align('center')
    date_format.set_right()
    center = workbook.add_format({'align': 'center'})
    center.set_align('center')
    
    for project_name, days in projects.iteritems():
        print user_name, project_name
        sheet = workbook.add_worksheet(format_filename(project_name))
        sheet_header = user_name + ", " + project_name + ", " + start_of_month.strftime('%B %Y')
        sheet.set_header(sheet_header) 
        sheet.write('A1', 'date', bold) 
        sheet.write('B1', 'topic', bold) 
        
        start_column = 2
        for day, worklog in sorted(days.iteritems()):
            if worklog:
                topic = all_issues[worklog.issueId].fields.summary
                print day, topic
                date = datetime.strptime(day, target_date_format)
                sheet.write_datetime('A' + str(start_column), date, date_format)
                sheet.write('B' + str(start_column), topic)
            
                start_column += 1

        formula = '=COUNT(A2:A' + str(start_column-1) + ') & " mandays"'
        sheet.write('B' + str(start_column), formula)
        sheet.set_column('A:A', 15)
        sheet.set_column('B:B', 50)
        
    workbook.close()
